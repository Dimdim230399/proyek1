package DimPujLen.proyek1.UangKu

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.setting.*
import kotlinx.android.synthetic.main.setting.view.*


class fragmensetting : Fragment(),View.OnClickListener {
    override fun onClick(v: View?) {
       when(v?.id){
           R.id.editKat->{
               fr = activity!!.supportFragmentManager.beginTransaction()
               fr.replace(R.id.FrameLayout,settingkat).commit()
               fr.addToBackStack(null)
           }

           R.id.button->{
               fr = activity!!.supportFragmentManager.beginTransaction()
               fr.replace(R.id.FrameLayout,ubahpasword).commit()
               fr.addToBackStack(null)

           }
           R.id.buttonlap->{
              // fr = activity!!.supportFragmentManager.beginTransaction()
               //fr.replace(R.id.FrameLayout,laporan).commit()
               //fr.addToBackStack(null)
               var inten = Intent(activity!!,Laporan::class.java)
               startActivity(inten)

           }


       }
    }

    lateinit var v : View
    lateinit var fr : FragmentTransaction
    lateinit var laporan: Laporan
    lateinit var thisParent : fragmenInti
    lateinit var settingkat: settingkat
    lateinit var ubahpasword: ubahpasword
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as fragmenInti
        settingkat = settingkat()
        ubahpasword = ubahpasword()
        laporan = Laporan()
        v = inflater.inflate(R.layout.setting,container,false)
        v.editKat.setOnClickListener(this)
        v.button.setOnClickListener(this)
        v.buttonlap.setOnClickListener(this)



        return  v

    }

}