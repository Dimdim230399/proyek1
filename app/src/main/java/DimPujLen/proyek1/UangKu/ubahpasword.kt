package DimPujLen.proyek1.UangKu

import android.app.AlertDialog
import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.password_activity.*
import kotlinx.android.synthetic.main.password_activity.view.*

class ubahpasword: Fragment(), View.OnClickListener {

    lateinit var  v : View
    lateinit var  thisparent : fragmenInti
    lateinit var db : SQLiteDatabase
    val user : Int = 1
    lateinit var  dialog : AlertDialog.Builder
    //lateinit var fr : FragmentTransaction
    //lateinit var login : activityLogin
    lateinit var preferece : SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.password_activity,container,false)
        thisparent =  activity as fragmenInti
        db = thisparent.getdatUang()
        dialog = AlertDialog.Builder(thisparent)
        v.button2.setOnClickListener(this)

    return v
    }



    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button2->{
                var sql = "select password from user where id_user ='$user'"
                val c : Cursor = db.rawQuery(sql,null)
                c.moveToFirst()
               var aa = c.getString(0)
                if (edtpass.text.toString() == aa  ){
                    //Toast.makeText(activity!!.baseContext,"benar",Toast.LENGTH_SHORT).show()

                    if(pasru.text.toString() == null && konfirpass.text.toString()== null){
                        Toast.makeText(activity!!.baseContext,"anda belum mengisi password baru anda",Toast.LENGTH_SHORT).show()
                    }
                    if (pasru.text.toString() == konfirpass.text.toString()){
                        dialog.setTitle("Notifikasi").setIcon(android.R.drawable.ic_dialog_info)
                            .setMessage("Apakah Anda Yakin!!")
                            .setPositiveButton("Yakin", action)
                            .setNegativeButton("Ngak", gak )
                        dialog.show()

                    }else{
                        konfirpass.setText("")
                        Toast.makeText(activity!!.baseContext,"Konfirmasi yang anda masukan tidak sama dengan password baru anda",Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(activity!!.baseContext,"Pasword anda salah",Toast.LENGTH_SHORT).show()
                    edtpass.setText("")
                }


            }
        }
    }

    fun update(password :String){
        val cv : ContentValues = ContentValues()
        cv.put("password", password)
        db.update("user",cv,"id_user ='$user'",null)
    }

    val action = DialogInterface.OnClickListener { dialog, which ->
        update(konfirpass.text.toString())

        preferece =  activity!!.getSharedPreferences("setting", Context.MODE_PRIVATE)
        val edit = preferece.edit()
            edit.putString("hind",konfirpass2.text.toString())
            edit.commit()
        pasru.setText("")
        konfirpass.setText("")
        edtpass.setText("")

        val inten = Intent(activity!!, activityLogin::class.java)
        startActivity(inten)
    }
    val gak = DialogInterface.OnClickListener { dialog, which ->
        Toast.makeText(activity!!.baseContext,"Pin tidak jadi di ganti",Toast.LENGTH_LONG).show()
        pasru.setText("")
        konfirpass.setText("")
        edtpass.setText("")
    }


}