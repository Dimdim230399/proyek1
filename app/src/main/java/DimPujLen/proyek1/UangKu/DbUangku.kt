package DimPujLen.proyek1.UangKu

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbUangku(context: Context):SQLiteOpenHelper(context,Db_name,null,Db_ver) {
    companion object{
        val Db_name = "Uangku"
        val Db_ver =1
    }


    override fun onCreate(db: SQLiteDatabase?) {
    val tblkategor = "create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null)"
    val tbluser = "create table user(id_user integer primary key autoincrement , password text not null)"
    val tbljenis = "create table jenis(id_jenis integer primary key autoincrement, nama_jenis text not null,id_kategori text not null)"
    val tbltrans = "create table transaksi(id_trans integer primary key autoincrement, id_jenis text not null , id_kategori text not null,tgl date not null ,uang int not null,keterangan text not null,bulan text not null)"
    val inskat  = "insert into kategori(nama_kategori) values ('pengeluaran'),('pemasukan')"
    val insjen = "insert into jenis(nama_jenis,id_kategori) values ('belanja','1'),('bonus','2')"
     val insus = "insert into user(password) values ('dimpujlen')"
      val instr = "insert into transaksi('id_jenis','id_kategori','tgl','uang','keterangan','bulan') values ('2','2','','0','','0'), ('1','1','-','0',' ','0')"
    db?.execSQL(tblkategor)
    db?.execSQL(tbluser)
    db?.execSQL(tbljenis)
    db?.execSQL(tbltrans)
     db?.execSQL(inskat)
     db?.execSQL(insjen)
    db?.execSQL(insus)
        db?.execSQL(instr)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}