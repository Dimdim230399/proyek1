package DimPujLen.proyek1.UangKu



import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.transaksi.*
import kotlinx.android.synthetic.main.transaksi.view.*

class fragmenTransaksi : Fragment(), View.OnClickListener {
        lateinit var  fr  :  FragmentTransaction
        lateinit var thisparent : fragmenInti
        lateinit var fragEdit: fragEdit
        lateinit var Detail : Detail
        lateinit var v : View
        lateinit var adappemasukan : SimpleCursorAdapter
        lateinit var preferece : SharedPreferences

        var id_jen :String =""
        lateinit var build : AlertDialog.Builder
        lateinit var db : SQLiteDatabase
        lateinit var  adaplist : SimpleCursorAdapter
        var bb = "0"
        var aa ="0"
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
            thisparent = activity as fragmenInti
            fragEdit = fragEdit()
            Detail = Detail()
            build = AlertDialog.Builder(thisparent)
            db = thisparent.getdatUang()
            v = inflater.inflate(R.layout.transaksi,container,false)
            v.tam.setOnClickListener(this)
            v.lsprod.setOnItemClickListener(clik)
            v.button.setOnClickListener(this)
            registerForContextMenu(v.lsprod)
            return v


        }
    fun delete(id_jen : String){
        db.delete("transaksi","keterangan = '$id_jen'",null)

        showtrans()
        pemasukan()

    }
    val indel = DialogInterface.OnClickListener { dialog, which ->
        delete(id_jen)

    }

    override fun onStart() {
        super.onStart()
        showtrans()
       pemasukan()
    }
    fun showtrans(){

        var  sql = "select a.nama_jenis as _id, b.nama_kategori , c.tgl, c.uang , c.keterangan from transaksi as c,jenis a,kategori b where c.id_jenis = a.id_jenis and c.id_kategori = b.id_kategori"
        val c: Cursor = db.rawQuery(sql, null)
        adaplist = SimpleCursorAdapter(
            thisparent,
            R.layout.isi,
            c,
            arrayOf( "tgl","uang","keterangan"),
            intArrayOf( R.id.tgl3,R.id.uang,R.id.keterangan),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsprod.adapter = adaplist
    }

    fun pemasukan(){
        var sql = "select sum(uang) as _id from transaksi where id_kategori='1'"
        var sqlkel = "select sum(uang) as _id from transaksi where id_kategori='2'"
        val c : Cursor= db.rawQuery(sql,null)
        val b : Cursor = db.rawQuery(sqlkel,null)
        c.moveToFirst()
        b.moveToFirst()
        //while (!c.isAfterLast){
        aa =c.getString(0)
        bb = b.getString(0)



        if (aa.toInt()== null && bb.toInt()==null){
            v.currentBudget.setText("Rp .0")
        }else if (aa.toInt() ==null){
            v.currentBudget.setText("Rp"+bb.toString())
        }else if (bb.toInt() == null){
            v.currentBudget.setText("Rp-"+aa.toString())
        } else if (aa.toInt() > 0 && bb.toInt() < 0  ) {
            v.currentBudget.setText("Rp"+ aa.toString() )
        }else if (aa.toInt() < 0 && bb.toInt() > 0  ) {
            v.currentBudget.setText("Rp"+ bb.toString() )
        }else if (aa.toInt() > bb.toInt()  ) {
            var d = bb.toInt()-aa.toInt()
            v.currentBudget.setText("Rp"+ d.toString() )
        }else if (aa.toInt() < bb.toInt()  ) {
            var f = bb.toInt()-aa.toInt()
            v.currentBudget.setText("Rp"+ f.toString() )
        } else{
            v.currentBudget.setText("Rp 0" )

        }



    }

    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_jen = c.getString(c.getColumnIndex("keterangan"))
        preferece =  activity!!.getSharedPreferences("setting",Context.MODE_PRIVATE)
        val edit = preferece.edit()
        edit.putString("aa",id_jen)
        edit.commit()
        Toast.makeText(activity!!.baseContext,id_jen,Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
      when(v?.id){
          R.id.tam->{
              fr = activity!!.supportFragmentManager.beginTransaction()
              fr.replace(R.id.FrameLayout,fragEdit).commit()
              fr.addToBackStack(null)

          }
          R.id.button->{
              build.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                  .setMessage("Apakah anda Yakin Menghapus Data ini !!!")
                  .setPositiveButton("Ya", indel)
                  .setNegativeButton("Tidak", null)
              build.show()
          }
      }

    }


    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
       activity!!.menuInflater.inflate(R.menu.det,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemDetail->{
                fr = activity!!.supportFragmentManager.beginTransaction()
                fr.replace(R.id.FrameLayout,Detail).commit()
                fr.addToBackStack(null)

                return true
            }
        }

        return  super.onContextItemSelected(item)
    }


}


