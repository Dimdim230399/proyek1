package DimPujLen.proyek1.UangKu


import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.isi_laporan.*
import kotlinx.android.synthetic.main.laporan.*
import java.util.*

// return inflater.inflate(R.layout.laporan,container,false)
    //}
class Laporan : AppCompatActivity (){


      //lateinit var v: View
      //lateinit var thisparent: fragmenInti
      var bulan = 0
      var tahun = 0
      var hari = 0
      var cal: Calendar = Calendar.getInstance()
      var param1 : String =""
      var param2 : String =""
    lateinit var adaplist :SimpleCursorAdapter
    lateinit var db : SQLiteDatabase
      var pilih : Int = 0
      lateinit var adapterspin : ArrayAdapter<String>
      var array_bulan = arrayOf("Pilih Bulan","Januari","February","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember")


      override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState)
          setContentView(R.layout.laporan)
          getdataUang()
          adapterspin = ArrayAdapter(this,android.R.layout.simple_list_item_1,array_bulan)
          spinner.adapter = adapterspin

          spinner.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
              override fun onNothingSelected(parent: AdapterView<*>?) {
                  Toast.makeText(baseContext,"Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()

              }

              override fun onItemSelected(
                  parent: AdapterView<*>?,
                  view: View?,
                  position: Int,
                  id: Long
              ) {
                  pilih = position
                  show_laporan_bulanan(pilih)
                  pengeluaran(pilih)
                  pemasukan(pilih)
                  Toast.makeText(baseContext,adapterspin.getItem(position),Toast.LENGTH_SHORT).show()

              }
          }

        /**
          button4.setOnClickListener{
              val datePickerDialog =
                  DatePickerDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->

                      button4.text= "$dayOfMonth" +
                              "-${monthOfYear + 1}-$year"
                      bulan = monthOfYear + 1
                      tahun = year
                      hari = dayOfMonth
                      param1 =  "$dayOfMonth" +
                              "-${monthOfYear + 1}-$year"


                  }, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))
              datePickerDialog.onDateSetListener
              datePickerDialog.setTitle("Masukan Tanggal Awal")
              datePickerDialog.setAccentColor(resources.getColor(R.color.colorPrimary))
              datePickerDialog.setOkText("Pilih")
              datePickerDialog.setCancelText("Batal")
              datePickerDialog.show(this.fragmentManager,"DatePickerDialog")
          }
          button5.setOnClickListener{
              val datePickerDialog =
                  DatePickerDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->

                      button5.text= "$dayOfMonth" +
                              "-${monthOfYear + 1}-$year"
                      bulan = monthOfYear + 1
                      tahun = year
                      hari = dayOfMonth
                      param2 ="$dayOfMonth" +
                              "-${monthOfYear + 1}-$year"

                  }, cal.get(Calendar.DAY_OF_MONTH ), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))
              datePickerDialog.onDateSetListener
              datePickerDialog.setTitle("Masukan Tanggal Akhir")
              datePickerDialog.setAccentColor(resources.getColor(R.color.colorPrimary))
              datePickerDialog.setOkText("Pilih")
              datePickerDialog.setCancelText("Batal")
              datePickerDialog.show(this.fragmentManager,"DatePickerDialog")
          }
     */


      }



    fun getdataUang() : SQLiteDatabase{
        db = DbUangku(this).writableDatabase
        return db
    }

    fun show_laporan_bulanan(bulan : Int){
        var  sql = "select a.nama_jenis as _id, b.nama_kategori , c.tgl, c.uang , c.keterangan from transaksi as c,jenis a,kategori b where c.id_jenis = a.id_jenis and c.id_kategori = b.id_kategori and c.bulan='$bulan'"
        val c: Cursor = db.rawQuery(sql,null)
        adaplist = SimpleCursorAdapter(
            this,
            R.layout.isi_laporan,
            c,
            arrayOf( "tgl","uang","keterangan","_id","nama_kategori"),
            intArrayOf( R.id.tg,R.id.txUang,R.id.textView11,R.id.kateg,R.id.jnis),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        listLap.adapter = adaplist

    }

    fun pengeluaran(bulan : Int){
        var sql ="select sum(uang) from transaksi where id_jenis='1' and bulan='$bulan'"
        val c  : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        textView12.setText("Rp"+c.getInt(0))

    }

    fun pemasukan(bulan : Int){
        var sql ="select sum(uang) from transaksi where id_jenis='2' and bulan='$bulan'"
        val c  : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        textView9.setText("Rp"+c.getInt(0))

    }



   // fun show_laporan_mingguan(){
    //    var  sql = "select a.nama_jenis as _id, b.nama_kategori , c.tgl, c.uang , c.keterangan from transaksi as c,jenis a,kategori b where c.id_jenis = a.id_jenis and c.id_kategori = b.id_kategori and  c.bulan='1'"
     //   val c: Cursor = db.rawQuery(sql,null)
      //  adaplist = SimpleCursorAdapter(
      //      this,
       //     R.layout.isi_laporan,
        //    c,
         //   arrayOf( "tgl","uang","keterangan","_id","nama_kategori"),
          //  intArrayOf( R.id.tg,R.id.txUang,R.id.textView11,R.id.kateg,R.id.jnis),
           // CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        //)
        //listLap.adapter = adaplist

    }







