package DimPujLen.proyek1.UangKu

import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.detail.view.*

class Detail : Fragment(),View.OnClickListener {

    lateinit var fr : FragmentTransaction
    lateinit var v : View
    lateinit var db :SQLiteDatabase
    lateinit var fragmenTransaksi: fragmenTransaksi
    lateinit var  thisparent : fragmenInti
    lateinit var pref : SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.detail,container, false)
        thisparent = activity as fragmenInti
        pref = activity!!.getSharedPreferences("setting",Context.MODE_PRIVATE)
        db = thisparent.getdatUang()
        fragmenTransaksi = fragmenTransaksi()
       // v.txtgl.setText("${pref.getString("aa","")}")
        var isian = pref.getString("aa","")
        tampil(isian.toString())
        v.btnok.setOnClickListener(this)
        return  v


    }

    fun tampil (kun : String){
        var  sql = "select a.nama_jenis as _id, b.nama_kategori , c.tgl, c.uang , c.keterangan from transaksi as c,jenis a,kategori b where c.id_jenis = a.id_jenis and c.id_kategori = b.id_kategori and c.keterangan='$kun'"
        val c: Cursor = db.rawQuery(sql, null)
        c.moveToFirst()

        v.txtgl.setText("Tanggal\t :" + c.getString(2))
        v.txuang.setText("jumlah Uang\t :"+c.getString(3))
        v.txKet.setText("Keterangan\t:"+ c.getString(4))
        v.txKategor.setText("Kategori\t:"+c.getString(1))
        v.txJen.setText("Jenis\t:"+c.getString(0))



    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnok->{
                fr = activity!!.supportFragmentManager.beginTransaction()
                fr.replace(R.id.FrameLayout,fragmenTransaksi).commit()
                fr.addToBackStack(null)
                Toast.makeText(activity!!.baseContext,"OK",Toast.LENGTH_SHORT).show()
            }
        }
    }
}